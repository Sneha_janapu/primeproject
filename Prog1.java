package Prime;

import java.util.Scanner;

public class Prog1 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter array size");
		int size = sc.nextInt();
		System.out.println("enter the elements:");
		int arr[] = new int[size];
		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}
		checkOccurance(arr);
	}
	public static void checkOccurance(int[] arr) {

		for (int i = 0; i < arr.length; i++) {
			int count = 1;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					count++;
					arr[j] = 'A';
				}
			}
			if (arr[i] != 'A')
		System.out.println(arr[i] + " occurs " + count + " times");
		}
	}
}
