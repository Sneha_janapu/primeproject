package Prime;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class TargetElementsSum {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of the array");

		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements of the array");

		for (int i = 0; i < size; i++) {

			arr[i] = sc.nextInt();
		}
		System.out.println("enter the target element");
		int tar = sc.nextInt();

		System.out.println(isCheckSum(arr, tar));
	}

	public static boolean isCheckSum(int[] arr, int tar) {
		Set<Integer> s = new HashSet<Integer>();

		for (int i = 0; i < arr.length; i++) {
			int temp;
			temp = tar - arr[i];
			if (s.contains(temp)) {
				return true;
			}
			s.add(arr[i]);
		}
		return false;
	}
}
